#!/usr/bin/python3

import subprocess
    
images = open('/tmp/demo.txt', 'w')
subprocess.call(['sudo', 'docker', 'images'], stdout=images)

images = open('/tmp/demo.txt').readlines()
image_id = []
for image in images:
    lines = image.strip()
    parts = lines.split(' ')
    while '' in parts:
        parts.remove('')
    if parts[0].startswith('gitlab') or parts[0].startswith('REPOSITORY'):
        pass
    else:
        image_id.append(parts[2])

for image in image_id:
    subprocess.call(['sudo', 'docker', 'rmi', '-f', image])

subprocess.call('rm', '-rf', '/tmp/demo.txt')

